﻿# Git 'er Done 🤠

## Installation
To install, simply run the following commands in the desired location on your machine.

    $ git clone https://gitlab.com/hassaan.ahmed1/git-erdone.git
    $ cd git-erdone && echo "export PATH=$PWD:\$PATH" >> ~/.zshrc && source ~/.zshrc

## Usage
To begin, provide a branch name and a new branch with that name will be checked out.

    $ git erdone <branch name starting with story id>

Next, provide a commit message whenever you want to commit. Use the optional flag to link the commit to Shortcut.

    $ git erdone [-l/--link] "Commit message"

Pushing is easy. Use the optional flag to copy a link to the story in Shortcut to your clipboard (convenient for writing the MR).

    $ git erdone push [-mr/--mr]

And finally, when you're done working on the branch

    $ git erdone done

That's it!
